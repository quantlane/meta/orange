foo = long_function_name(var_one, var_two,
					     var_three, var_four)
my_list = [
	1, 2, 3,
	4, 5, 6,
	]
result = some_function_taking_a_list(
	123, [
		'a', 'b', 'c',
		'd', 'e', 'f',
	])
def some_function(
	argument1: str,
	argument2: int,
	argument3: decimal.Decimal,
	argument4: Optional[List[str]],
	argument5: Optional[List[str]],
) -> None:
	'''
	Docstring
	'''
	pass
def some_function_long(
	argument1: str,
	argument2: int,
	argument3: decimal.Decimal,
	argument4: Optional[List[str]],
	argument5: Optional[List[str]],
) -> Union[List[Any], Tuple[Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any], Dict[Any, Any]]:
	'''
	Docstring
	'''
	pass


# output


foo = long_function_name(var_one, var_two, var_three, var_four)
my_list = [
	1,
	2,
	3,
	4,
	5,
	6,
]
result = some_function_taking_a_list(
	123,
	[
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
	],
)


def some_function(
	argument1: str,
	argument2: int,
	argument3: decimal.Decimal,
	argument4: Optional[List[str]],
	argument5: Optional[List[str]],
) -> None:
	'''
	Docstring
	'''
	pass


def some_function_long(
	argument1: str,
	argument2: int,
	argument3: decimal.Decimal,
	argument4: Optional[List[str]],
	argument5: Optional[List[str]],
) -> Union[
	List[Any],
	Tuple[Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any, Any],
	Dict[Any, Any],
]:
	'''
	Docstring
	'''
	pass
