a = lambda package_name, keep_major_version=None: cq_version
a = lambda package_name, keep_major_version=None, arg=None, *args: cq_version
b = lambda _: 1
e = lambda x = None: 1
f = lambda *x: True
f = lambda *_: True
f = lambda **x: True
f = lambda **_: True
f = lambda *_x, **_y: True


# output


a = lambda package_name, keep_major_version = None: cq_version
a = lambda package_name, keep_major_version = None, arg = None, *args: cq_version
b = lambda _: 1
e = lambda x = None: 1
f = lambda *x: True
f = lambda *_: True
f = lambda **x: True
f = lambda **_: True
f = lambda *_x, **_y: True
