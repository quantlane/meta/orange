import pathlib
class X:
	def __init__(self):
		pass
	def x(self):
		def y():
			pass
		return y()
def z():
	...


# output


import pathlib


class X:
	def __init__(self):
		pass

	def x(self):
		def y():
			pass

		return y()


def z():
	...
