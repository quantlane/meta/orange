class A:
    """"""
    pass
class B:
    """
    """
    pass
class X:
    """Oneline docstring"""
    pass
class Y:
    """
    Docstring on separate line
    """
    pass
class YY:
    """
    Multiline docstring
    Without special indentation
    """
    pass
class Z:
    """
    Multiline docstring
        With some indenation
    """
    pass
class W:
    """Multiline docstring that starts like this
    but continues like this
    """
    pass


# output


class A:
	''' '''

	pass


class B:
	''' '''

	pass


class X:
	''' Oneline docstring '''

	pass


class Y:
	'''
	Docstring on separate line
	'''

	pass


class YY:
	'''
	Multiline docstring
	Without special indentation
	'''

	pass


class Z:
	'''
	Multiline docstring
	    With some indenation
	'''

	pass


class W:
	'''
	Multiline docstring that starts like this
	but continues like this
	'''

	pass
