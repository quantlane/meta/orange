# Tests readme

## What do we want to test
Since `orange` is a fork of black, we do not necessarily want to test that black functions as expected.
We want to test that changes that we made to the black are behaving as we want and merging changes from
upstream does not causes any problems.

## Generating tests
1. Create file in `orange_tests/data/` e.g. `mytest.py`
2. Paste or write any valid python code there:
    ```python
    def function(
        arg1: List[str],
        arg2: int
    ):
        """ Docstring for function.
        """
        return arg1+[  arg2  ]
    ```
3. Spawn poetry shell `poetry shell`
3. Run `PYTHONPATH=./src/ ./orange_tests/generate_test_example.py mytest.py`
4. The file will be changed to this:
    ```python
    def function(
        arg1: List[str],
        arg2: int
    ):
        """: Docstring for function.
        """
        return arg1+[  arg2  ]


    TODO
    ```
    Section of code after comment *#output* is code above formatted by orange.
5. If you are satisfied with the output you are done! Otherwise modify the output part until you think it is appropriate and start a discussion about changing the `orange`.

## Running tests
1. Go to `orange_tests/`
2. Run `PYTHONPATH=../src/ poetry run py.test`
